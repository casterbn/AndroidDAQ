package com.tmrnavi.sensordaq;

import android.content.Context;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.stream.IntStream;

public class PSINSDataStore {
    private static String filename;
    private static String filepath;
    private static File file;
    private static FileOutputStream fos;
    private static Context context;

    public PSINSDataStore(Context mainContext){
        context = mainContext;
    }

    public void open(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
        Date date = new Date(System.currentTimeMillis());
        filename = "PSINS_" + simpleDateFormat.format(date) + ".bin";
        filepath = context.getExternalFilesDir(null)+"//"+filename;
        file = new File(filepath);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            fos = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void close(){
        try {
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // AHRS 存储
    public void writedata(float[] wib, float[] fsf, float[] mag, float[] att, float[] qnb, float t){
        new Thread(new Runnable() {
            @Override
            public void run() {
                String line = String.format("%.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f\n",
                        wib[0], wib[1], wib[2], fsf[0], fsf[1], fsf[2], mag[0], mag[1], mag[2], att[0], att[1], att[2],
                        qnb[0], qnb[1], qnb[2], qnb[3], t);
                try {
                    fos.write(line.getBytes());
                    fos.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    // AHRS 存储
    public void writedata(float[] wib, float[] fsf, float[] mag, double[] gnssdata, double[] avpxp, float t){
        String line = wib[0] +" "+ wib[1] +" "+ wib[2] +" "+ fsf[0] +" "+ fsf[1] +" "+ fsf[2] +" "+ mag[0] +" "+ mag[1] +" "+ mag[2] +" "+
                gnssdata[2] +" "+ gnssdata[3] +" "+ gnssdata[4] +" "+ gnssdata[5] +" "+ gnssdata[6] +" "+ gnssdata[7] +" "+ gnssdata[8] +" "+
                avpxp[0] +" "+ avpxp[1] +" "+ avpxp[2] +" "+ avpxp[3] +" "+ avpxp[4] +" "+ avpxp[5] +" "+ avpxp[6] +" "+ avpxp[7] +" "+ avpxp[8] +" "+
                avpxp[9] +" "+ avpxp[10] +" "+ avpxp[11] +" "+ avpxp[12] +" "+ avpxp[13] +" "+ avpxp[14] +" "+ avpxp[15] +" "+ avpxp[16] +" "+ avpxp[17] +" "+
                avpxp[18] +" "+ avpxp[19] +" "+ avpxp[20] +" "+ avpxp[21] +" "+ avpxp[22] +" "+ avpxp[23] +" "+ avpxp[24] +" "+ avpxp[25] +" "+ avpxp[26] +" "+
                avpxp[27] +" "+ avpxp[28] +" "+ avpxp[29] +" "+ avpxp[30] +" "+ avpxp[31] +" "+ avpxp[32] +" "+ avpxp[33] +" "+ avpxp[34] +" "+ avpxp[35] +" "+
                avpxp[36] +" "+ avpxp[37] +" "+ avpxp[38] +" "+ avpxp[39] +" "+ avpxp[40] +" "+ avpxp[41] +" "+ gnssdata[1] +" "+ t + "\n";
        try {
            fos.write(line.getBytes());
            fos.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writebindata(float[] wib, float[] fsf, float[] mag, double[] gnssdata, double[] avpxp, float t) {
        double[] buffer = new double[60];
        IntStream.range(0, 3).forEach(i -> {
            buffer[i] = (double) wib[i];
            buffer[i + 3] = (double)fsf[i];
            buffer[i + 6] = (double)mag[i];
        });
        System.arraycopy(gnssdata, 2, buffer, 9, 7);
        System.arraycopy(avpxp, 0, buffer, 16, 41);
        buffer[58] = gnssdata[1];
        buffer[59] = (double)t;
        for (int i=0; i<60; i++) {
            try {
                byte[] buf = doubletoBytes(buffer[i]);
                fos.write(buf);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void writebindata(float[] sensors, double[] gnssdata, double[] nmeadata, float t) {
        double[] buffer = new double[29];
        for (int i=0; i<10; i++) buffer[i] = sensors[i];
        System.arraycopy(gnssdata, 0, buffer, 10, 8);
        System.arraycopy(nmeadata, 0, buffer, 18, 10);
        buffer[28] = (double)t;
        for (int i=0; i<29; i++) {
            try {
                byte[] buf = doubletoBytes(buffer[i]);
                fos.write(buf);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private byte [] convertDoubleToByteArray(double number) {
        ByteBuffer byteBuffer = ByteBuffer.allocate(Double.BYTES);
        byteBuffer.putDouble(number);
        byteBuffer.flip();                // 翻转
        return byteBuffer.array();
    }

    private static byte[] doubletoBytes(double dblValue) {
        long data = Double.doubleToRawLongBits(dblValue);
        return new byte[]{
                (byte) ((data >> 56) & 0xff),
                (byte) ((data >> 48) & 0xff),
                (byte) ((data >> 40) & 0xff),
                (byte) ((data >> 32) & 0xff),
                (byte) ((data >> 24) & 0xff),
                (byte) ((data >> 16) & 0xff),
                (byte) ((data >> 8) & 0xff),
                (byte) ((data >> 0) & 0xff),
        };
    }
}
