# Android定位传感器数据读取

#### 介绍
使用Android手机读取运动传感器数据

<img src="https://images.gitee.com/uploads/images/2021/0912/203611_e104de5d_8748713.png" width="300px">

#### 软件架构
建立传感器监听器、位置监听器和NMEA消息监听器，实现手机运动数据读取。
因为Android API无法实现精确定时输出，所以建立定时任务对采样周期内输出的传感器数据进行平均，程序中设计的采样频率为100Hz。
程序中的所有数据会以二进制形式保存类double类型，数据定义为：
|序号|定义|单位|
|--|--|--|
|0~2|角速度|deg/s|
|3~5|加速度|m/s^2|
|6~8|磁场强度|uT|
|9|气压|hPa|
|10|UTC时间|hhmmss|
|11|纬度|deg|
|12|经度|deg|
|13|海拔|m|
|14|定位精度|m|
|15|航向|deg|
|16|水平速度|m/s|
|17|速度精度|m/s|
|18|NMEAUTC时间|hhmmss|
|19|NMEA纬度|ddmm.mm|
|20|NMEA经度|ddmm.mm|
|21|GNSS解类型||
|22|卫星数量||
|23|DOP||
|24|海拔高度|m|
|25|高度修正|m|
|26|航向|deg|
|27|水平速度|m/s|
|28|系统时间|s|


#### 使用说明
安装提供定位权限就可以完成启动软件采集数据，点击保存按钮，数据会存储在手机外部存储空间，对应路径为：手机型号/Android/data/com.tmrnavi.sensordaq/files/PSINS_XXXX.bin
